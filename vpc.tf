resource "aws_vpc" "VPC_curso" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames
  tags = {
    Name    = var.tag_name
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "curso_pub1" {
  vpc_id                  = aws_vpc.VPC_curso.id
  cidr_block              = var.publicsCIDRblock1
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZonePublic1
  tags = {
    Name    = "Public subnet 1"
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "curso_pub2" {
  vpc_id                  = aws_vpc.VPC_curso.id
  cidr_block              = var.publicsCIDRblock2
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZonePublic2
  tags = {
    Name    = "Public subnet 2"
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "curso_pri1" {
  vpc_id                  = aws_vpc.VPC_curso.id
  cidr_block              = var.privatesCIDRblock1
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZonePrivate1
  tags = {
    Name    = "Private subnet 1"
    projeto = var.tag_projeto
  }
}
resource "aws_subnet" "curso_pri2" {
  vpc_id                  = aws_vpc.VPC_curso.id
  cidr_block              = var.privatesCIDRblock2
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZonePrivate2
  tags = {
    Name    = "Private subnet 2"
    projeto = var.tag_projeto
  }
}
resource "aws_network_acl" "Public_NACL" {
  vpc_id     = aws_vpc.VPC_curso.id
  subnet_ids = [aws_subnet.curso_pub1.id, aws_subnet.curso_pub2.id]
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.publicdestCIDRblock
    from_port  = 22
    to_port    = 22
  }


  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.publicdestCIDRblock
    from_port  = 22
    to_port    = 22
  }

  tags = {
    Name    = "Public NACL"
    projeto = var.tag_projeto
  }
}
resource "aws_internet_gateway" "IGW_curso" {
  vpc_id = aws_vpc.VPC_curso.id
  tags = {
    Name    = "Internet gateway curso"
    projeto = var.tag_projeto
  }
}
resource "aws_route_table" "Public_RT_curso" {
  vpc_id = aws_vpc.VPC_curso.id
  tags = {
    Name    = "Public Route table"
    projeto = var.tag_projeto
  }
}
resource "aws_route" "internet_access_curso" {
  route_table_id         = aws_route_table.Public_RT_curso.id
  destination_cidr_block = var.publicdestCIDRblock
  gateway_id             = aws_internet_gateway.IGW_curso.id
}
resource "aws_route_table_association" "Public_association_1" {
  subnet_id      = aws_subnet.curso_pub1.id
  route_table_id = aws_route_table.Public_RT_curso.id
}
resource "aws_route_table_association" "Public_association_2" {
  subnet_id      = aws_subnet.curso_pub2.id
  route_table_id = aws_route_table.Public_RT_curso.id
}
